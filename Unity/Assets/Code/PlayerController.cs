using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	[SerializeField] private float speed = 10.0f;
	[SerializeField] private Transform head;
	private CharacterController characterController;

	private void Awake()
	{
		characterController = GetComponent<CharacterController>();
		Cursor.lockState = CursorLockMode.Locked;
	}

    private void Update()
	{
		//transform.Translate(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));

		Vector3 move = Vector3.zero;
		move += Input.GetAxis("Horizontal") * transform.right;
		move += Input.GetAxis("Vertical") * transform.forward;
		characterController.SimpleMove(move * speed);
		transform.Rotate(Vector3.up, Input.GetAxis("Mouse X"));
		head.Rotate(Vector3.right, Input.GetAxis("Mouse Y"));
	}
}
