#pragma once
#include <d3d11.h>
#include <DirectXMath.h>

using namespace DirectX;

class Light
{
public:
	enum LightType
	{
		Directional = 0,
		Point = 1
	};

	struct LightData
	{
		//XMFLOAT3A lightDirection; // only light direction for directional light only
		XMFLOAT4 lightWorldPosition; // light world position, w -> 0.0f - directional light, 1.0 - point light
		XMFLOAT4 ambientLight;
		XMFLOAT4 diffuseLight;
		XMFLOAT4 specularLight;
		XMFLOAT4 emissionLight;
		FLOAT lightIntensity;
		XMFLOAT3 padding; // because of 16 byte alignement

		LightData(XMFLOAT3 _lightWorldPosition, LightType lightType) : ambientLight(0.0f, 0.0f, 0.0f, 0.0f), diffuseLight(0.0f, 0.0f, 0.0f, 0.0f), specularLight(0.0f, 0.0f, 0.0f, 0.0f), emissionLight(0.0f, 0.0f, 0.0f, 0.0f), lightIntensity(0.0f)
		{
			lightWorldPosition = { _lightWorldPosition.x, _lightWorldPosition.y, _lightWorldPosition.z, 0.0f };
			switch (lightType)
			{
			case Light::Directional:
			default:
				lightWorldPosition.w = 0.0f;
				break;

			case Light::Point:
				lightWorldPosition.w = 1.0f;
				break;
			}
		}
	};

	INT init(ID3D11Device* pD3DDevice, LightData &light);
	void render(ID3D11DeviceContext* pD3DDeviceContext);
	void deInit();

private:
	ID3D11Buffer* _pLightBuffer = nullptr; // constant buffer with light data
};

