#include "Window.h"

LRESULT CALLBACK WndProc(
	HWND hWnd, // handle to window instance
	UINT msg, // message id
	WPARAM wParam, // main information
	LPARAM lParam // additional informations
)
{
	switch (msg)
	{
	case WM_CLOSE:
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		if (wParam == VK_ESCAPE) PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	return 0;
}

INT Window::init(HINSTANCE hInstance, INT width, INT height, INT nCmdShow)
{
	// 1. describe window class
	WNDCLASS wc = {};
	wc.hInstance = hInstance;
	wc.hbrBackground = CreateSolidBrush(RGB(255, 0, 255)); // handle to background color brush
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW); // handle to cursor icon
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION); // handle to application icon
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // not visual style, but window behaviour
	wc.lpszClassName = TEXT("Direct3D 11"); // window class name
	wc.lpfnWndProc = WndProc;

	// 2. register window class
	if (!RegisterClass(&wc)) return 10;

	// 3. calculate window size (optional)
	INT screenWidth = GetSystemMetrics(SM_CXSCREEN);
	INT screenHeight = GetSystemMetrics(SM_CYSCREEN);
	DWORD style = WS_OVERLAPPEDWINDOW;
	RECT r = { (screenWidth - width) / 2, (screenHeight - height) / 2, (screenWidth + width) / 2, (screenHeight + height) / 2 }; // left-top corner, right-bottom corner
	AdjustWindowRect(&r, style, false);

	// 4. create window instance
	_hWnd = CreateWindow(
		wc.lpszClassName, // window class name
		wc.lpszClassName, // window title
		style, // visual window style
		r.left, r.top, // left-top corner
		r.right - r.left, r.bottom - r.top, // window size
		nullptr, // parent window (optional)
		nullptr, // handle to menu (optional)
		hInstance,
		nullptr // optional parameters
	);
	if (_hWnd == nullptr) return 15;

	// 5. show window
	ShowWindow(_hWnd, nCmdShow); // bring window to front
	SetFocus(_hWnd); // set keyboard focus

	return 0;
}

BOOL Window::run()
{
	static MSG msg = {};
	if (PeekMessage(&msg, nullptr, 0, UINT_MAX, PM_REMOVE))
	{
		TranslateMessage(&msg); // translate keyboard messages for virtual key tables
		DispatchMessage(&msg); // send message to window procedure
	}

	return msg.message != WM_QUIT;
}

void Window::deInit()
{
}
