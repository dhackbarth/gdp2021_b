cbuffer MatrixBuffer
{
    matrix worldViewProjectionMatrix;
};

struct VertexInput
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
};

struct VertexOutput
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD;
};

VertexOutput main(VertexInput INPUT)
{
    VertexOutput OUTPUT;
    
    OUTPUT.position = mul(float4(INPUT.position, 1.0f), worldViewProjectionMatrix);
    OUTPUT.uv = INPUT.uv * float2(1.0f, 1.0f) + float2(0.0f, 0.0f); // uv * tiling + offset
    
    return OUTPUT;
}