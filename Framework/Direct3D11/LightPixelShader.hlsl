Texture2D MainTexture;
sampler MainSampler;

cbuffer LightData
{
    //float3 lightDirection; // only light direction for directional light only
    float4 lightWorldPosition; // light world position, w -> 0.0f - directional light, 1.0 - point light
    float4 ambientLight;
    float4 diffuseLight;
    float4 specularLight;
    float4 emissionLight;
    float lightIntensity;
};

struct PixelInput
{
    float4 position : SV_POSITION;
    float3 positionWorld : POSITION1;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
    float3 view : TEXCOORD1;
};

float4 main(PixelInput INPUT) : SV_TARGET
{
    float4 textureColor = MainTexture.Sample(MainSampler, INPUT.uv);
    float4 ambientColor = ambientLight;
    float4 diffuseColor = 0;
    float4 specularColor = 0;
    float4 emissionColor = emissionLight;
    
    // light source
    float3 normal = normalize(INPUT.normal);
    //float3 light = normalize(lightDirection); // old version
    float3 light = lightWorldPosition.xyz - INPUT.positionWorld * lightWorldPosition.w;
    float lightLength = length(light);
    light /= lightLength;
    float attenuation = 1.0f / (1.0f + (0.2f * lightLength + 0.1f * lightLength * lightLength) * lightWorldPosition.w);
    
    // diffuse color
    float diffuse = max(dot(normal, light), 0); // dot(n, l) = cos of angle between n and l
    diffuseColor = diffuseLight * (diffuse * lightIntensity * attenuation);
    
    // specular color with Phong light model
    //float3 reflectVector = reflect(-light, normal);
    //float3 view = normalize(INPUT.view);
    //float phongExponent = 512.0f;
    //float specular = pow(max(dot(reflectVector, view), 0), phongExponent);
    //specularColor = specularLight * (specular * lightIntensity);
    
    // specular color with BlinnPhong light model
    float3 view = normalize(INPUT.view);
    float3 halfVector = normalize(light + view);
    float phongExponent = 1024.0f;
    float specular = pow(max(dot(halfVector, normal), 0), phongExponent);
    specularColor = specularLight * (specular * lightIntensity * attenuation);

    // texture * (ambient + diffuse) + specular + emission
    return saturate(textureColor * saturate(ambientColor + diffuseColor) + specularColor + emissionLight);
}