#pragma once
#include <Windows.h>
#include <chrono>

using namespace std;

#define TIMEPOINT chrono::high_resolution_clock::time_point
typedef chrono::high_resolution_clock::time_point time_point;
typedef chrono::duration<float> duration;

class Time
{
public:
	INT init();
	void update();
	void deInit();

	FLOAT getDeltaTime() { return _deltaTime; }

private:
	time_point _lastTimestamp = {}; // timestamp of last frame
	FLOAT _deltaTime = 0.0f; // time between two frames
	FLOAT _totalTime = 0.0f; // total time since startup
	INT _fpsCount = 0; // frame count since last frame update
	FLOAT _fpsTime = 0.0f; // time since last frame update
};

