#pragma once

#define CheckError(error) if (error != 0) return error;
#define CheckFailed(hr, error) if (FAILED(hr)) return error;

template<typename T>
void safeRelease(T * &obj)
{
	if (obj != nullptr)
	{
		obj->Release();
		obj = nullptr;
	}
}