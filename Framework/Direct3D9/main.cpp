#include <Windows.h>
#include <random>
#include "Window.h"
#include "D3D.h"
#include "Mesh.h"
#include "Camera.h"
#include "Time.h"
#include "Material.h"
#include "Light.h"
#include "Utils.h"

int WINAPI WinMain(
	HINSTANCE hInstance, // handle to our application instance
	HINSTANCE hPrevInstance, // deprecated
	PSTR szCmdLine, // command line
	INT nCmdShow // how the window should be shown
)
{
	INT error = 0;
	INT width = 1024;
	INT height = 768;
	BOOL isFullscreen = false;
	
	// initialise phase
	// 1. create a window
	Window wnd = {};
	error = wnd.init(hInstance, width, height, nCmdShow);
	CheckError(error);

	// 2. create Direct3D 9 connection
	D3D d3d = {};
	error = d3d.init(wnd.getWindowHandle(), width, height, isFullscreen);
	CheckError(error);

	d3d.getDevice()->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	d3d.getDevice()->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	d3d.getDevice()->SetRenderState(D3DRS_LIGHTING, TRUE);
	d3d.getDevice()->SetRenderState(D3DRS_SPECULARENABLE, TRUE);

	// 3. create mesh
	Mesh mesh = {};
	error = mesh.init(d3d.getDevice());
	CheckError(error);

	// 4. create camera
	Camera camera = {};
	error = camera.init(width, height);
	CheckError(error);

	// 5. create time
	Time time = {};
	error = time.init();
	CheckError(error);

	// 6. create material
	Material material = {};
	error = material.init(d3d.getDevice(), TEXT("wall.jpg"));
	CheckError(error);

	// 7. create light
	Light light = {};
	D3DLIGHT9 lightData = {};
	lightData.Type = D3DLIGHT_DIRECTIONAL;
	lightData.Direction = { -1.0f, -1.0f, 1.0f };
	lightData.Ambient = { 0.2f, 0.2f, 0.2f, 1.0f };
	lightData.Diffuse = { 0.6f, 0.6f, 0.6f, 1.0f };
	lightData.Specular = { 1.0f, 1.0f, 1.0f, 1.0f };
	error = light.init(lightData);
	
	// run phase
	while (wnd.run())
	{
		// update objects
		time.update();
		mesh.update(time.getDeltaTime());

		// draw objects
		// random color
		//static std::default_random_engine e;
		//static std::uniform_int_distribution<int> d(0, 255);
		//d3d.beginScene(D3DCOLOR_XRGB(d(e), d(e), d(e)));

		d3d.beginScene(D3DCOLOR_XRGB(0, 0, 0));

		camera.render(d3d.getDevice());
		material.render(d3d.getDevice());
		light.render(d3d.getDevice());
		mesh.render(d3d.getDevice());

		d3d.endScene();
	}

	// deinitialise phase
	light.deInit();
	material.deInit();
	time.deInit();
	camera.deInit();
	mesh.deInit();
	d3d.deInit();
	wnd.deInit();

	return 0;
}